{ ... }:

{
  age.secrets."ssh-keys/nix-build.mollontinaxial.net".file =
    ../secrets/ssh-keys/nix-build.mollontinaxial.net.age;

  nix = {
    buildMachines = [{
      hostName = "nix-build.mollontinaxial.net";
      system = "x86_64-linux";
      maxJobs = 4;
      supportedFeatures = [ "benchmark" "big-parallel" "nixos-test" "kvm" ];
      sshKey = "/run/secrets/ssh-keys/nix-build.mollontinaxial.net";
    }];

    distributedBuilds = true;

    extraOptions = ''
      builders-use-substitutes = true
    '';
  };
}
