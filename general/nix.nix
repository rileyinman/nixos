{ self, nixpkgs, hostname, pkgs, ... }:

{
  nix = {
    # Enable flake support
    extraOptions = ''
      experimental-features = nix-command flakes
    '';

    # Allow all users in wheel group to manage Nix
    settings = {
      allowed-users = [ "@wheel" ];
      trusted-users = [ "@wheel" ];
    };

    # Automatically delete generations older than two (2) weeks to save space
    gc = {
      automatic = true;
      options = "--delete-older-than 14d";
    };

    # Optimise the nix store automatically in the background to save space
    optimise.automatic = true;

    nixPath = [
      "nixpkgs=${nixpkgs}"
      "nixos-config=/run/current-system/config/machines/${hostname}/configuration.nix"
    ];
  };

  # Link the current config into a static path for NIX_PATH
  system.extraSystemBuilderCmds = ''
    ln -s '${../.}' "$out/config"
  '';

  # Automatically upgrade inputs and rebuild
  system.autoUpgrade = {
    enable = true;
    flake = self.outPath;
    flags = [
      "--recreate-lock-file"
      "--commit-lock-file"
    ];
  };

  # Allow the listing and installation of nonfree software
  nixpkgs.config.allowUnfree = true;
}
