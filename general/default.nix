{ ... }:

{
  imports = [
    ./declarative-users.nix
    ./filesystems.nix
    ./fonts.nix
    ./keyboard.nix
    ./locale.nix
    ./man.nix
    ./nix.nix
    ./pam-keyinit.nix
  ];
}
