{ lib, ... }:

{
  security.pam.services.login.text = lib.mkDefault (
    lib.mkAfter ''
      # Links user keyring into session keyring
      session optional pam_keyinit.so force revoke
    ''
  );
}
