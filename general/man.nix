{ pkgs, ... }:

{
  environment.systemPackages = [ pkgs.man-pages ];

  # Enables extra manpages
  documentation.dev.enable = true;
}
