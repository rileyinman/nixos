{ ... }:

{
  console.useXkbConfig = true;

  services.xserver.xkb = {
    layout = "us";
    variant = "dvorak";
    options = "ctrl:nocaps,compose:rctrl";
  };
}
