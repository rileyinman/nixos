{ ... }:

{
  age.secrets."pam/u2f_keys" = {
    file = ../secrets/pam/u2f_keys.age;
    mode = "0444";
  };

  security.pam.u2f = {
    enable = true;
    control = "sufficient";
    authFile = "/run/secrets/pam/u2f_keys";
  };
}
