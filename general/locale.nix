{ ... }:

{
  i18n = {
    defaultLocale = "en_US.UTF-8";
    # Set time to 24-hour, display seconds
    extraLocaleSettings.LC_TIME = "C.UTF-8";
  };
}
