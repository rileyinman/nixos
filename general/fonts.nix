{ pkgs, ... }:

{
  console.font = "${pkgs.terminus_font}/share/fonts/terminus/ter-x16n.pcf.gz";

  fonts = {
    enableDefaultPackages = true;
    enableGhostscriptFonts = true;
  };
}
