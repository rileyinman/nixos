{ ... }:

{
  imports = [
    ./cli-utils.nix
    ./git.nix
    ./htop.nix
    ./neovim.nix
    ./sudo.nix
  ];
}
