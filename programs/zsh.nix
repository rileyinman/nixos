{ ... }:

{
  programs.zsh.enable = true;

  # Enable zsh completion for system applications
  environment.pathsToLink = [ "/share/zsh" ];
}
