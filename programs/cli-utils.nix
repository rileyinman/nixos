{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    exfat
    fd
    file
    ripgrep
  ];
}
