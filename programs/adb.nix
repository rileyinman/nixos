{ pkgs, ... }:

{
  programs.adb.enable = true;

  systemd.services.adb = {
    description = "Start the Android Debug Daemon";
    serviceConfig = {
      Type = "forking";
      ExecStart = "${pkgs.androidenv.androidPkgs_9_0.platform-tools}/bin/adb start-server";
      ExecStop = "${pkgs.androidenv.androidPkgs_9_0.platform-tools}/bin/adb kill-server";
    };
    wantedBy = [ "multi-user.target" ];
  };
}
