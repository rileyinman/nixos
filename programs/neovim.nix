{ ... }:

{
  # Remove nano
  environment.defaultPackages = [];

  programs.neovim = {
    enable = true;
    vimAlias = true;
    viAlias = true;
    defaultEditor = true;
  };
}
