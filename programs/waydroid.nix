{ ... }:

{
  # Fix an issue until it's added to the module config
  boot.kernel.sysctl = {
    "kernel.unprivileged_bpf_disabled" = 0;
  };

  virtualisation.waydroid.enable = true;
}
