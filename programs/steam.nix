{ ... }:

{
  # Enable OpenGL and 32-bit libraries (needed for steam)
  hardware.opengl = {
    enable = true;
    driSupport32Bit = true;
  };
}
