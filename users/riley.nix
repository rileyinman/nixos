{ config, lib, pkgs, ... }:

{
  imports = [
    ../programs/sudo.nix
    ../programs/zsh.nix
  ];

  users.extraUsers.riley = {
    description = "Riley Inman";
    extraGroups = with lib; mkMerge [
      [
        "wheel" # Enable sudo
        "dialout" # Enable serial access
      ]

      (mkIf config.programs.adb.enable [
        "adbusers"
      ])
    ];
    isNormalUser = true;
    shell = pkgs.zsh;
  };
}
