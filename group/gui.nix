{ ... }:

{
  imports = [
    ../general/input.nix
    ../general/pipewire.nix
    ../services/gvfs.nix
    ../services/printing.nix
  ];
}
