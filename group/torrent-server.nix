{ ... }:

{
  imports = [
    ./server.nix
    ../services/deluge.nix
    ../services/jackett.nix
    ../services/lidarr.nix
    ../services/radarr.nix
    ../services/sonarr.nix
  ];
}
