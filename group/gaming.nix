{ ... }:

{
  imports = [
    ../programs/steam.nix
    ../udev/steam-hardware.nix
    ../udev/switch.nix
  ];
}
