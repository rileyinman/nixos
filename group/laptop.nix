{ ... }:

{
  imports = [
    ./gui.nix
    ../general/suspend-then-hibernate.nix
    ../programs/brightnessctl.nix
    ../udev/autosuspend.nix
  ];
}
