{ ... }:

{
  services.udev.extraRules = ''
    # NXP LPC55 ROM bootloader (unmodified)
    SUBSYSTEM=="hidraw", ATTRS{idVendor}=="1fc9", ATTRS{idProduct}=="0021", TAG+="uaccess", MODE:="0666"
    # NXP LPC55 ROM bootloader (with Solo 2 VID:PID)
    SUBSYSTEM=="hidraw", ATTRS{idVendor}=="1209", ATTRS{idProduct}=="b000", TAG+="uaccess", MODE:="0666"
    # Solo 2
    SUBSYSTEM=="tty", ATTRS{idVendor}=="1209", ATTRS{idProduct}=="beee", TAG+="uaccess", MODE:="0666"
    # Solo 2
    SUBSYSTEM=="usb", ATTRS{idVendor}=="1209", ATTRS{idProduct}=="beee", TAG+="uaccess", MODE:="0666"
  '';
}
