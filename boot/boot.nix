{ ... }:

{
  boot = {
    # Fancy boot screen
    plymouth.enable = true;

    # Skip bootloader by default
    loader.timeout = null;
  };
}
