{ ... }:

{
  imports = [ ./boot.nix ];

  # Use the systemd-boot EFI boot loader.
  boot.loader = {
    systemd-boot.enable = true;
    systemd-boot.editor = true;
    efi.canTouchEfiVariables = true;
  };
}
