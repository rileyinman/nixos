{
  services.rpcbind.enable = true;

  systemd.mounts = [
    {
      type = "nfs";
      mountConfig = {
        Options = "noatime";
      };
      what = "hm-lin-fs01.internal.mpalf.net:/mnt/main_pool/media";
      where = "/srv/store/Media";
    };
  ];

  systemd.automounts = let
    commonAutoMountOptions = {
      wantedBy = [ "multi-user.target" ];
      automountConfig = {
        TimeoutIdleSec = "600";
      };
    };
  in [
    (commonAutoMountOptions // { where = "/srv/store/Media"; })
  ];
}
