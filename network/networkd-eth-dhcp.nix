{ ... }:

{
  imports = [ ./networkd-eth.nix ];

  systemd.network.networks."20-wired" = {
    networkConfig.DHCP = "yes";
  };
}
