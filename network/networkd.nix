{ lib, ... }:

{
  imports = [ ./resolved.nix ];

  networking = {
    dhcpcd.enable = false;
    useDHCP = false;
    useNetworkd = true;
    timeServers = [
      "time.cloudflare.com"
      "time.nist.gov"
      "0.pool.ntp.org"
      "1.pool.ntp.org"
    ];
  };

  systemd = {
    network.enable = true;
    services.systemd-networkd-wait-online.enable = lib.mkForce false;
  };

  systemd.network.networks."10-unmanaged" = {
    matchConfig.Name = "lo";
    linkConfig.Unmanaged = true;
  };
}
