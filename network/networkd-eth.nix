{ ... }:

{
  systemd.network.networks."20-wired" = {
    matchConfig.Name = "en* eth*";
    networkConfig.IPv6PrivacyExtensions = "yes";
    dhcpV4Config.UseDomains = "yes";
  };
}
