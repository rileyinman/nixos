{ ... }:

{
  systemd.network.networks."25-wireless" = {
    matchConfig.Type = "wlan";
    networkConfig.DHCP = "yes";
    networkConfig.IPv6PrivacyExtensions = "yes";
    dhcpV4Config.UseDomains = "yes";
  };
}
