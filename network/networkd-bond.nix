{ ... }:

{
  # NOTE: The device must be bond1 instead of bond0. The kernel
  # automatically creates bond0 upon loading the bond network module,
  # and systemd doesn't touch the options of pre-existing netdevs.
  systemd.network.netdevs.bond1 = {
    netdevConfig = {
      Name = "bond1";
      Kind = "bond";
    };
    bondConfig = {
      Mode = "active-backup";
      PrimaryReselectPolicy = "always";
      MIIMonitorSec = "1s";
    };
  };

  systemd.network.networks."10-bond1" = {
    matchConfig.Name = "bond1";
    networkConfig.DHCP = "yes";
    networkConfig.IPv6PrivacyExtensions = "yes";
    dhcpV4Config.UseDomains = "yes";
  };

  systemd.network.networks."20-wired-bond" = {
    matchConfig.Name = "en* eth*";
    networkConfig.Bond = "bond1";
    networkConfig.PrimarySlave = true;
  };

  systemd.network.networks."25-wireless-bond" = {
    matchConfig.Type = "wlan";
    networkConfig.Bond = "bond1";
  };
}
