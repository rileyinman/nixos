{ nixos-hardware, ... }:

{
  imports = [
    nixos-hardware.nixosModules.common-cpu-intel-kaby-lake
    nixos-hardware.nixosModules.lenovo-thinkpad-x1-6th-gen

    ./hardware-configuration.nix

    ../../group/laptop.nix
    ../../group/gaming.nix

    ../../hardware/bluetooth.nix
    # ../../hardware/ocl.nix

    ../../boot/efi-boot.nix

    ../../network/iwd.nix
    ../../network/networkd.nix
    ../../network/networkd-bond.nix
    ../../network/networkd-eth-dhcp.nix
    ../../network/networkd-wlan.nix

    # ../../general/distributed-build.nix
    ../../general/pam-u2f.nix
    ../../general/swapfile.nix
    ../../general/virtualization.nix

    ../../desktops/sway.nix

    ../../programs/adb.nix
    ../../programs/waydroid.nix

    ../../services/avahi.nix
    ../../services/fwupd.nix
    ../../services/geoclue2.nix
    ../../services/openvpn.nix

    ../../udev/switch.nix
  ];

  users.extraUsers.riley.hashedPassword = "$6$QXCbQ0FA2QqiMk$Cj0DpuZHKsGwno34Q00x9eJxr8PvO2UVXTraTRSdqpMabzowdsCqeM.Aq0tu7Q2ZscbnOH0BlJCc0Vt6prWVA1";

  boot.resumeDevice = "/dev/disk/by-uuid/9251da67-3751-47d7-a370-28a2825f24d3";
  boot.kernelParams = [ "resume_offset=46278656" ];

  # Enable fingerprint reader registration for the user
  services.udev.extraRules = ''
    SUBSYSTEMS=="usb" ATTRS{idVendor}=="06cb", ATTRS{idProduct}=="009a", MODE:="0666"
  '';

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "21.11"; # Did you read the comment?
}
