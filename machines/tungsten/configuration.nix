{ pkgs, nixos-hardware, ... }:

{
  imports = [
    ./hardware-configuration.nix

    ../../group/gaming.nix

    ../../hardware/bluetooth.nix

    ../../boot/efi-boot.nix

    ../../network/networkd.nix
    ../../network/networkd-eth-dhcp.nix

    ../../general/swapfile.nix
    ../../general/virtualization.nix

    ../../desktops/sway.nix

    ../../services/fwupd.nix
  ];

  users.extraUsers.riley.hashedPassword = "$6$QXCbQ0FA2QqiMk$Cj0DpuZHKsGwno34Q00x9eJxr8PvO2UVXTraTRSdqpMabzowdsCqeM.Aq0tu7Q2ZscbnOH0BlJCc0Vt6prWVA1";

  networking.hostName = "tungsten"; # Define your hostname.

  # Set your time zone.
  time.timeZone = "America/Los_Angeles";
  
  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

}
