{ modulesPath, ... }:

{
  imports = [
    # <nixpkgs/nixos/modules/virtualisation/lxc-container.nix>
    "${modulesPath}/virtualisation/lxc-container.nix"
    ./hardware-configuration.nix

    ../../group/server.nix

    ../../network/disable-firewall.nix
    ../../network/networkd.nix
    ../../network/networkd-eth.nix

    ../../programs/sudo.nix
  ];

  users.extraUsers.riley.hashedPassword = "$6$ZcdewpBZWiWjy3eQ$Pq9RFO/OnPmli90Z8SQmefxKLC/90HNplRzUvO39uCAyXVksCA43mvnvWYm/rs3tp9V44Uts/6VDXNndxAmfQ0";

  networking.useHostResolvConf = false;

  age.secrets.networkd-static-addr = {
    file = ../../secrets/networkd/ma-lin-nb01-static-addr.age;
    path = "/etc/systemd/network/50-static-addr.network";
    mode = "0444";
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?
}
