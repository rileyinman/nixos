{ ... }:

{
  services.radarr.enable = true;

  # Allow access to deluge downloads
  users.extraUsers.radarr.extraGroups = [ "deluge" ];
}
