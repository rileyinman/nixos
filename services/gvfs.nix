{ pkgs, ... }:

{
  services.gvfs = {
    enable = true;

    # For some reason, the default gvfs package is gnome.gvfs
    #   which pulls in a lot of GNOME and GTK dependencies.
    # We want the smaller package.
    package = pkgs.gvfs;
  };
}
