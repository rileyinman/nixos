{ ... }:

{
  services.deluge = {
    enable = true;
    declarative = true;
    authFile = "/etc/nixos/secrets/deluge/auth";
    config = {
      # Unknown
      del_copy_torrent_file = false;

      # Downloads
      download_location = "/var/lib/deluge/Downloads";
      move_completed = false;
      move_completed_path = "/var/lib/deluge/Downloads";
      copy_torrent_file = false;
      torrentfiles_location = "/var/lib/deluge/Downloads";
      autoadd_enable = false;
      compact_allocation = false;
      prioritize_first_last_pieces = false;
      add_paused = false;

      # Network
      random_port = true;
      listen_ports = [ 6881 6891 ];
      random_outgoing_ports = true;
      outgoing_ports = [ 0 0 ];
      listen_interface = "";
      peer_tos = "0x00";
      upnp = true;
      natpmp = true;
      utpex = true;
      lsd = true;
      dht = true;

      # Encryption
      enc_in_policy = true;
      enc_out_policy = true;
      enc_level = 2;
      enc_prefer_rc4 = true;

      # Bandwidth
      max_connections_global = 200;
      max_upload_slots_global = 4;
      max_download_speed = -1.0;
      max_upload_speed = -1.0;
      max_half_open_connections = 50;
      max_connections_per_second = 20;
      ignore_limits_on_local_network = true;
      rate_limit_ip_overhead = true;
      max_connections_per_torrent = -1;
      max_upload_slots_per_torrent = -1;
      max_download_speed_per_torrent = -1.0;
      max_upload_speed_per_torrent = -1.0;

      # Other
      # update notifications?
      send_info = false;
      geoip_db_location = "/usr/share/GeoIP/GeoIp.dat";

      # Daemon
      daemon_port = 58846;
      allow_remote = true;
      new_release_check = false;

      # Queue
      auto_managed = true;
      queue_new_to_top = false;
      max_active_limit = 8;
      max_active_downloading = 3;
      max_active_seeding = 5;
      dont_count_slow_torrents = false;
      share_ratio_limit = 2.0;
      seed_time_ratio_limit = 7.0;
      seed_time_limit = 180;
      stop_seed_at_ratio = false;
      stop_seed_ratio = 2.0;
      remove_seed_at_ratio = false;

      # Proxy
      proxies = {
        peer = {
          username = "";
          password = "";
          hostname = "";
          type = 0;
          port = 8080;
        };
        web_seed = {
          username = "";
          password = "";
          hostname = "";
          type = 0;
          port = 8080;
        };
        tracker = {
          username = "";
          password = "";
          hostname = "";
          type = 0;
          port = 8080;
        };
        dht = {
          username = "";
          password = "";
          hostname = "";
          type = 0;
          port = 8080;
        };
      };

      # Cache
      cache_size = 512;
      cache_expiry = 60;

      # Plugins
      plugins_location = "/var/lib/deluge/.config/deluge/plugins";
      enabled_plugins = [ "Label" ];
    };

    web.enable = true;
  };
}
