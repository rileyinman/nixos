{ ... }:

{
  services.sonarr.enable = true;

  # Allow access to deluge downloads
  users.extraUsers.sonarr.extraGroups = [ "deluge" ];
}
