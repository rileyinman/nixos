{ pkgs, ... }:

{
  age.secrets = {
    "openvpn/pia/config".file = ../secrets/openvpn/pia/config.age;
    "openvpn/pia/psk".file = ../secrets/openvpn/pia/psk.age;

    "openvpn/zrz/config".file = ../secrets/openvpn/zrz/config.age;
    "openvpn/zrz/psk".file = ../secrets/openvpn/zrz/psk.age;

    "openvpn/zrz-internal/config".file = ../secrets/openvpn/zrz-internal/config.age;
    "openvpn/zrz-internal/psk".file = ../secrets/openvpn/zrz-internal/psk.age;
  };

  services.openvpn.servers.pia = {
    up = "${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved";
    down = "${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved";
    config = "config /run/secrets/openvpn/pia/config";
    autoStart = false;
  };

  services.openvpn.servers.zrz = {
    up = "${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved";
    down = "${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved";
    config = "config /run/secrets/openvpn/zrz/config";
    autoStart = false;
  };

  services.openvpn.servers.zrz-internal = {
    up = "${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved";
    down = "${pkgs.update-systemd-resolved}/libexec/openvpn/update-systemd-resolved";
    config = "config /run/secrets/openvpn/zrz-internal/config";
    autoStart = false;
  };

  systemd.network.networks."10-tun-ignore" = {
    matchConfig.Name = "tun*";
    linkConfig.Unmanaged = true;
  };
}
