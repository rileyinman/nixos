{ ... }:

{
  services.jackett.enable = true;

  # Fix jackett binding to ipv6
  systemd.tmpfiles.rules = [
    #  Path                                              Mode UID     GID     Age Argument
    "f /var/lib/jackett/.config/Jackett/appsettings.json -    jackett jackett -   { \"urls\": \"http://0.0.0.0:9117\" }\\n"
  ];
}
