{ ... }:

{
  services.nfs.server = {
    enable = true;
    createMountPoints = true;
  };
}
