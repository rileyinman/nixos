{ ... }:

{
  services.lidarr.enable = true;

  # Allow access to deluge downloads
  users.extraUsers.lidarr.extraGroups = [ "deluge" ];
}
