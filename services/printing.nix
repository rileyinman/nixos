{ pkgs, ... }:

{
  services.printing = {
    enable = true;
    drivers = with pkgs; [
      gutenprint
      hplip
    ];
  };

  environment.systemPackages = [ pkgs.system-config-printer ];
}
