{ ... }:

{
  security.acme.acceptTerms = true;
  security.acme.email = "rileyminman@gmail.com";

  services.nginx = {
    enable = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;
    recommendedOptimisation = true;
    recommendedGzipSettings = true;

    resolver.addresses = [ "127.0.0.53:53" ];

    commonHttpConfig = ''
      map $host $theme {
        default dark;
      }
    '';
  };
}
