{ upkgs, ... }:

{
  services.plex = {
    enable = true;
    package = upkgs.plex;
  };
}
