{ pkgs, ... }:

{
  environment.systemPackages = [ pkgs.agenix ];
  age.secretsDir = "/run/secrets";
}
