let
  riley-carbide = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKVwiVXR1vGVFd9f4D4TGhrACsaU15dc/ib8CtDCWndO";
  riley-ma-lin-nb01 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICyqlZEHpbYIrc+WnZrERNWPJ5a3pCGo75YlpPkqhoN6";

  users = [
    riley-carbide
    riley-ma-lin-nb01
  ];

  carbide = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMMtKqVSKxWrw7YatYwr5TRWmVjG9NrAG+2oZ9BWQUcC";
  ma-lin-nb01 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM4FkbCVDQjCN4rUVZGBy4IraG3QLYDT5nSu1ZmXcpel";

  machines = [
    carbide
    ma-lin-nb01
  ];
in
{
  # Network
  "networkd/ma-lin-nb01-static-addr.age".publicKeys = [
    riley-ma-lin-nb01 ma-lin-nb01
  ];

  # OpenVPN
  "openvpn/pia/config.age".publicKeys = users ++ machines;
  "openvpn/pia/psk.age".publicKeys = users ++ machines;

  "openvpn/zrz/config.age".publicKeys = users ++ machines;
  "openvpn/zrz/psk.age".publicKeys = users ++ machines;

  "openvpn/zrz-internal/config.age".publicKeys = users ++ machines;
  "openvpn/zrz-internal/psk.age".publicKeys = users ++ machines;

  # Pam
  "pam/u2f_keys.age".publicKeys = [
    riley-carbide carbide
  ];

  # SSH
  "ssh-keys/nix-build.mollontinaxial.net.age".publicKeys = [
    riley-carbide carbide
  ];
}
