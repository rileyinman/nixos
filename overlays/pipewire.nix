_: prev:
{
  # Shamelessly pilfered from
  # https://discourse.nixos.org/t/how-to-override-pulseaudio-alsa-files/6718,
  # with modifications based on
  # https://nzeid.net/pulseaudio-disable-auto-volume
  # and made to work with pipewire
  pipewire-no-mic-boost = prev.pipewire.overrideAttrs(old: {
    pname = "${old.pname}-no-mic-boost";

    # Instead of overriding some post-build action, which would require a
    # pipewire rebuild, we override the entire `buildCommand` to produce its
    # outputs by copying the original package's files (much faster).
    buildCommand = /* ft=sh */ ''
      set -euo pipefail

      # Copy original files, for each split-output (`out`, `dev` etc.).
      ${prev.lib.concatStringsSep "\n"
        (map
          (outputName: /* ft=sh */ ''
              echo "Copying output ${outputName}"
              set -x
              cp -a ${prev.pipewire.${outputName}} ''$${outputName}
              set +x
          '')
          old.outputs
        )
      }

      # Replace:
      #   volume = merge
      # with:
      #   volume = zero
      # in Element Capture / Element Mic Boost and all variants.
      set -x
      # ${prev.perl}/bin/perl -i -0777pe 's%^\[Element.*(Capture|Mic Boost).*\](\n|.)*?volume = \K(merge)%zero%gm' $out/share/alsa-card-profile/mixer/paths/*
      ls $out/share/alsa-card-profile
      exit 2
      set +x
    '';
  });
}
