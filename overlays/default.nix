{ lib, nixpkgs-unstable, agenix, ... }:

{
  nixpkgs.overlays = with lib; mkMerge [
    (mkOrder 0 [(
      # Make unstable available as an overlay when necessary
      # NOTE: Avoid using if possible!
      _: prev:
      {
        unstable = import nixpkgs-unstable {
          inherit (prev) config;
          localSystem = prev.system;
          overlays = [];
        };
      }
    )])

    [
      agenix.overlays.default
      # ( import ./pipewire.nix )
      ( import ./sudo.nix )
      ( import ./torrents.nix )
      ( import ./video-accel.nix )
    ]
  ];
}
