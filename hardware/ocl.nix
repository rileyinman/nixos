{ pkgs, ... }:

{
  hardware.opengl.extraPackages = [ pkgs.intel-ocl ];
}
